const express = require('express');

const router = express.Router();

const { authMiddleware } = require('./middleware/authMiddleware');

const {
  registerUser, loginUser, getProfileInfo, deleteUser, changePass,
} = require('./usersService');

router.post('/register', registerUser);

router.post('/login', loginUser);

router.get('/me', authMiddleware, getProfileInfo);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me', authMiddleware, changePass);

module.exports = {
  usersRouter: router,
};
