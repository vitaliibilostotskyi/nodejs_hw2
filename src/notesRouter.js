const express = require('express');

const router = express.Router();
const {
  createNote, getNote, deleteUserNoteById, getProfileInfoNotes, updateMyNoteById,
  markMyNoteCompletedById,
} = require('./notesService');

router.post('/', createNote);

router.get('/', getProfileInfoNotes);

router.get('/:id', getNote);

router.put('/:id', updateMyNoteById);

router.patch('/:id', markMyNoteCompletedById);

router.delete('/:id', deleteUserNoteById);

module.exports = {
  notesRouter: router,
};
