const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

const { authMiddleware } = require('./middleware/authMiddleware');

const accesLogStream = fs.createWriteStream('access.log', { flags: 'a' });

mongoose.connect('mongodb+srv://BilostVit:MDB136661991@bilostvit.kowgkrt.mongodb.net/todoapp?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter');
const { usersRouter } = require('./usersRouter');

app.use(express.json());
app.use(morgan('combined', { stream: accesLogStream }));

app.use('/api/notes', authMiddleware, notesRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', usersRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.error(err);
  return res.status(500).send({ message: 'String' });
}

app.use(errorHandler);
