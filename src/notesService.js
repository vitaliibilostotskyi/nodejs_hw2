const { Note } = require('./models/Notes');

function createNote(req, res) {
  try {
    const { text } = req.body;
    const note = new Note({
      text,
      userId: req.user.userId,
      createdDate: new Date().toISOString(),
    });
    note.save().then(() => {
      res.status(200).json({ message: 'success' });
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
}

const getProfileInfoNotes = async (req, res) => {
  try {
    Note.find({ userId: req.user.userId }, '-__v').then((result) => {
      if (!result) {
        res.status(400).json({ message: 'No notes found' });
      } else {
        if (req.query.limit || req.query.offset) {
          const { offset = 0, limit = 0 } = req.query;
          res.status(200).json({
            offset,
            limit,
            count: result.length,
            notes: result.slice(offset, +offset + +limit),
          });
        } else {
          res.status(200).json({
          offset: 0,
          limit: 0,
          count: result.length,
          notes: result
        });
       }
      }
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const getNote = (req, res) => {
  try {
    Note.findOne({ _id: req.params.id, userId: req.user.userId })
      .then((note) => {
        if (!note) {
          res.status(400).json({ message: 'Access denied' });
        } else {
          res.status(200).json({ note });
        }
      });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const updateMyNoteById = (req, res) => {
  try {
    const { text } = req.body;
    return Note.findOneAndUpdate(
      { _id: req.params.id, userId: req.user.userId },
      { $set: { text } },
    )
      .then((result) => {
        if (!result) {
          return res.status(400).json({ message: 'Access denied' });
        }
        return res.status(200).json({ message: 'Note was updated' });
      });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const markMyNoteCompletedById = async (req, res) => {
  try {
    const note = await Note.findOne({ _id: req.params.id, userId: req.user.userId });
    if (!note) {
      return res.status(400).json({ message: 'Note is not found' });
    }
    note.completed = !note.completed;
    await note.save();
    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const deleteUserNoteById = async (req, res) => {
  try {
    const note = await Note.findOne({ _id: req.params.id, userId: req.user.userId });
    if (!note) {
      return res.status(400).json({ message: 'Note is not found' });
    }
    note.deleteOne()
      .then(() => res.status(200).json({ message: 'success' }));
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = {
  createNote,
  getNote,
  deleteUserNoteById,
  getProfileInfoNotes,
  updateMyNoteById,
  markMyNoteCompletedById,
};
